import pandas as pd
import plotly.graph_objects as go

# Read data in from CSV
# TODO read symbol from user input and chart it accordingly

def candles_chart(symbol=None):
    symbol = input("Please enter a symbol to chart:")
    candles = pd.read_csv(f"./data/kucoin/1h/{symbol}.csv")

    # Parse timestamps into readable datetimes
    candles['Date'] = pd.to_datetime(candles['Date'], unit='ms')

    #  Create plotly figure
    fig = go.Figure(data=[go.Candlestick(
                    x=candles['Date'],
                    open=candles['Open'],
                    high=candles['High'],
                    low=candles['Low'],
                    close=candles['Close']
    )])

    # Remove rangeslider
    fig.update_layout(xaxis_rangeslider_visible=False)
    fig.show()

candles_chart()