import os

from flask import Flask

from environs import Env

# Read .env file
env = Env()
env.read_env()

def create_app():
    app = Flask(__name__)
    app.config['SECRET_KEY'] = os.environ.get('SECRET_KEY') # Replace for env variables at a later stage

    # Import views and auth
    from .views import views
    from .auth import auth

    app.register_blueprint(views, url_prefix='/')
    app.register_blueprint(auth, url_prefix='/')

    return app