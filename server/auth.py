# All of our routes that need authentication
from flask import Blueprint, render_template, redirect, url_for, request

auth = Blueprint('auth', __name__)

@auth.route('/login', methods=['GET', 'POST'])
def login():
    username = request.form.get('username')
    password = request.form.get('password')
    return render_template('login.html')

@auth.route('/sign-up', methods=['GET', 'POST'])
def signup():
    # Get form data from POST
    username = request.form.get('username')
    password = request.form.get('password')
    confirm_password = request.form.get('confirm_password')
    return render_template('signup.html')

@auth.route('/logout')
def logut():
    return redirect(url_for('views.index'))