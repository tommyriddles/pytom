from exchange_stats import *

exchange_stats() #KuCoin
exchange_stats('binance', top=20)
exchange_stats('kraken')
# Following exchanged don't support OHLCV
# exchange_stats('btcmarkets')
# exchange_stats('coinspot')
# exchange_stats('independentreserve')
exchange_stats('bitfinex')
exchange_stats('bitmex')