import os
import ccxt
import pandas as pd

# Pass through an exchange and pull stats
def exchange_stats(exchange_id='kucoin', base_pair='USDT', timeframe='1h', top=10):
    try:
        # Use default exchange of kucoin
        exchange_class = getattr(ccxt, exchange_id)
        exchange = exchange_class({
            'timeout': 30000,
            'enableRateLimit': True,
        })
        markets = exchange.load_markets()
        pairs = []

        # Pull all pairs
        for pair in markets:
            if pair[-4:] == base_pair:
                pairs.append(str(pair)) # Add any matches to the dict
        
        # Pull ticker price into a dataframe if base_pair had a hit
        if len(pairs) == 0:
            return print(f'{base_pair} had not matches on {exchange}')

        print(f'{base_pair} data found on {exchange}')
        tickers = exchange.fetchTickers(pairs)
        tickersDf = pd.DataFrame.from_dict(tickers, orient='index')

        # Clean DataFrame
        tickersDf.drop(['timestamp', 'datetime', 'bidVolume', 'askVolume',
                        'open', 'previousClose', 'info'], axis=1, inplace=True)

        # Sort by Volume
        tickersDf.sort_values(by='quoteVolume', ascending=False, inplace=True)

        print('\n',tickersDf.head(top),'\n') # Display Top 10

        # Loop through the top X rated coins and pull OHLCV
        for index, row in tickersDf.head(n=top).iterrows():
            
            # Root data directory
            data = f'./data/{exchange_id}/{timeframe}/'

            # Look to see if data directory already exists
            if not os.path.exists(data):
                os.makedirs(data)

            # Replace forward slash with hyphen
            # fSymbol is for pulling data when you get the symbol from a already saved file
            symbol = row['symbol'].replace('/', '-')
            fSymbol = symbol.replace('-', '/')
            n1 = '\n'  # New line delimeter for use inside f-strings

            # Look for existing CSV files
            if os.path.exists(f'{data + symbol}.csv'):
                print(
                    f'Existing data found for {symbol} on the {timeframe} timeframe, loading into dataframe...')
                df = pd.read_csv(f'{data + symbol}.csv')
                # Adding 1 hour to the last candle: ADJUST ACCORDINGLY TO SUIT TIME PERIOD - Exchange uses milliseconds
                since = int(df['Date'][-1:] + 3600 * 1000)
                # Pull new candles
                candles = exchange.fetchOHLCV(fSymbol, timeframe, since=since)
                # No candles, notify and move forward
                if len(candles) == 0:
                    print(f'No new candles yet for {fSymbol}{n1}')
                # At least 1 new candle
                elif len(candles) >= 1:
                    # Add new candles to new DataFrame and append to current DataFrame
                    newCandles = pd.DataFrame(
                        candles, columns=['Date', 'Open', 'High', 'Low', 'Close', 'Volume'])
                    df = df.append(newCandles, ignore_index=True)
                    # Save DataFrame to CSV
                    df.to_csv(
                        path_or_buf=f'{os.path.join(data) + symbol}.csv', index=False)
                    print(
                        f'{len(newCandles)} new candles for {symbol} on the {timeframe} timeframe, saved to CSV.{n1}')

            # No data found for symbol, pull new OHLCV data and save
            if not os.path.exists(f'{data + symbol}.csv'):
                print(f'No data found for {symbol} on the {timeframe} timeframe, pulling it now...')
                candles = exchange.fetchOHLCV(fSymbol, timeframe)
                # Add candles to DataFrame
                df = pd.DataFrame(candles, columns=[
                                'Date', 'Open', 'High', 'Low', 'Close', 'Volume'])
                # Save DataFrame to CSV
                df.to_csv(
                    path_or_buf=f'{os.path.join(data) + symbol}.csv', index=False)
                print(f'{len(candles)} candles pulled for {symbol}, new data saved.')

    except (AttributeError, KeyError) as error:
        if AttributeError:
            print('Incorrect exchange passed to handler\n', error)
        elif KeyError:
            print('Incorrect timeframe passed to handler\n', error)

# exchange_stats('kucoin', 'USDT', '3.3m')
